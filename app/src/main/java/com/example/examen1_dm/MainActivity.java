package com.example.examen1_dm;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MainActivity extends AppCompatActivity {

    private TextView txtUsuario, txtContrasena;
    private Button btnIngresar, btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);
        this.init();

        this.btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtUsuario.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Usuario vacío", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(txtUsuario.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Contraseña vacía", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(!txtUsuario.getText().toString().equals(getString(R.string.user)) || !txtContrasena.getText().toString().equals(getString(R.string.pass))) {
                    Toast.makeText(getApplicationContext(), "Usuario o Contraseña incorrecto", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(getApplicationContext(), CalculadoraActivity.class);
                intent.putExtra("usuario", txtUsuario.getText().toString());
                startActivity(intent);
            }
        });

        this.btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    private void init() {
        this.txtUsuario = (EditText) findViewById(R.id.txtUsuario);
        this.txtContrasena = (EditText) findViewById(R.id.txtContrasena);
        this.btnIngresar = (Button) findViewById(R.id.btnIngresar);
        this.btnSalir = (Button) findViewById(R.id.btnSalir);
    }
}