package com.example.examen1_dm;

public class Calculadora {
    private float num1, num2;

    public Calculadora() {
        this.num1 = 0;
        this.num2 = 0;
    }

    public Calculadora(float num1, float num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    public float getNum1() {
        return num1;
    }

    public void setNum1(float num1) {
        this.num1 = num1;
    }

    public float getNum2() {
        return num2;
    }

    public void setNum2(float num2) {
        this.num2 = num2;
    }

    // Métodos
    public float suma() {
        return this.num1 + this.num2;
    }

    public float resta() {
        return this.num1 - this.num2;
    }

    public float multiplicacion() {
        return this.num1 * this.num2;
    }

    public float division() {
        return this.num1 / this.num2;
    }
}
