package com.example.examen1_dm;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class CalculadoraActivity extends AppCompatActivity {

    private TextView lblUsuario, lblResultado;
    private EditText txtNum1, txtNum2;

    private Button btnSuma, btnResta, btnMulti, btnDivi, btnLimpiar, btnRegresar;

    private Calculadora obj;

    private boolean isVacio() {
        return this.txtNum1.getText().toString().equals("") || this.txtNum2.getText().toString().equals("");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_calculadora);
        this.init();

        this.btnSuma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isVacio()) {
                    Toast.makeText(getApplicationContext(), "No debe haber campos vacíos", Toast.LENGTH_SHORT).show();
                    return;
                }
                obj.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                obj.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                lblResultado.setText(String.valueOf(obj.suma()));
            }
        });

        this.btnResta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isVacio()) {
                    Toast.makeText(getApplicationContext(), "No debe haber campos vacíos", Toast.LENGTH_SHORT).show();
                    return;
                }
                obj.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                obj.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                lblResultado.setText(String.valueOf(obj.resta()));
            }
        });

        this.btnMulti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isVacio()) {
                    Toast.makeText(getApplicationContext(), "No debe haber campos vacíos", Toast.LENGTH_SHORT).show();
                    return;
                }
                obj.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                obj.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                lblResultado.setText(String.valueOf(obj.multiplicacion()));
            }
        });

        this.btnDivi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isVacio()) {
                    Toast.makeText(getApplicationContext(), "No debe haber campos vacíos", Toast.LENGTH_SHORT).show();
                    return;
                }
                obj.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                obj.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                float value;
                try {
                    value = obj.division();
                    if(Float.isInfinite(value)) {
                        throw new Exception("Error");
                    }
                } catch (Exception err) {
                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                    return;
                }
                lblResultado.setText(String.valueOf(value));
            }
        });

        this.btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtNum1.setText("");
                txtNum2.setText("");
                lblResultado.setText("");
            }
        });

        this.btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    private void init() {
        this.obj = new Calculadora();
        this.lblUsuario = (TextView) findViewById(R.id.lblUsuario);
        Bundle usuario = getIntent().getExtras();
        String nombre = usuario.getString("usuario");
        this.lblUsuario.setText(nombre);
        this.lblResultado = (TextView) findViewById(R.id.lblResultado);
        this.txtNum1 = (EditText) findViewById(R.id.txtNum1);
        this.txtNum2 = (EditText) findViewById(R.id.txtNum2);
        this.btnSuma = (Button) findViewById(R.id.btnSuma);
        this.btnResta = (Button) findViewById(R.id.btnResta);
        this.btnMulti = (Button) findViewById(R.id.btnMultiplicacion);
        this.btnDivi = (Button) findViewById(R.id.btnDivision);
        this.btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        this.btnRegresar = (Button) findViewById(R.id.btnRegresar);
    }
}